<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CourseStatus extends Component
{
    use AuthorizesRequests;

    public $course, $current, $index, $previous, $next;

    public function mount(Course $course){
        $this->course = $course;

        foreach($course->lessons() as $lesson){
            if(!$lesson->completed){
                $this->current = $lesson;

                //Indice
                $this->index = $course->lessons->search($lesson);
                $this->previous = $course->lessons[$this->index - 1];
                $this->next = $course->lessons[$this->index - 1];
                break;
            }
        }

        if(!$this->current) {
            $this->current = $course->lessons->last();
        }

        $this->authorize('enrolled', $course);
    }

    public function render()
    {
        return view('livewire.course-status');
    }

    public function changeLesson(Lesson $lesson) {
        $this->current = $lesson;

        $this->index = $this->course->lessons->pluck('id')->search($lesson->id);

        if($this->index == 0){
            $this->previous = null;
        }else {
            $this->previous = $this->course->lessons[$this->index - 1];
        }

        if($this->index == $this->course->lessons->count() -1){
            $this->next = null;
        }else {
            $this->next = $this->course->lessons[$this->index + 1];
        }
    
    }

    public function completed() {
        if($this->current->completed) {
            //Eliminar registro
            $this->current->users()->detach(auth()->user()->id);
        }else {
            //agregar registro
            $this->current->users()->attach(auth()->user()->id);
        }

        $this->current = Lesson::find($this->current->id);
        $this->course = Course::find($this->course->id);
    }

    public function getAdvanceProperty(){
        $i = 0;
        foreach($this->course->lessons as $lesson){
            if($lesson->completed){
                $i++;
            }
        }

        $advance = ($i * 100) /($this->course->lessons->count());

        return round($advance, 2);
    }
}
