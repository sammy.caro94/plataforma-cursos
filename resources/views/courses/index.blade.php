<x-app-layout>
    <section class="bg-cover" style="background-image: url({{asset('img/cursos/learn.jpg')}})">
        
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-36">

            <div class="w-full md:w-3/4 lg:w-1/2">
                <h1 class="text-white font-bold text-4xl">Los mejores cursos en español!</h1>
                <p class="text-white text-lg mt-4 mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, similique deleniti a non, distinctio fugit ipsum quas quod corrupti modi, doloribus suscipit ex autem illum facilis iste hic amet atque.</p>
                
                @livewire('search');
      
           </div>
       </div>
    </section>

    @livewire('courses-index')
    
</x-app-layout>