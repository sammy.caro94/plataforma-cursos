<x-app-layout>

    <section class="bg-gray-700 py-12 mb-12">

        <div class="container grid grid-cols-1 lg:grid-cols-2 gap-6">

            <figure>
                <img src="{{Storage::url($course->image->url)}}" alt="" class="h-60 w-full object-cover">
                <div></div>
            </figure>

            <div class="text-white">
                <h1 class="text-4xl">{{$course->title}}</h1>
                <h2 class="text-xl mg-3">{{$course->subtitle}}</h2>
                <p class="mb-2"> <i class="fas fa-chart-line"></i> Nivel:{{$course->level->name}}</p>
                <p class="mb-2"> <i class=""></i> Categoria:{{$course->category->name}}</p>
                <p class="mb-2"> <i class="fas fa-users"></i> Matriculados:{{$course->students_count}}</p>
                <p> <i class="fas fa-star"></i> Calificación: {{$course->rating}}</p>
            </div>

        </div>

    </section>

     
        <section class="container">
            {{-- x-data="{ open: false }" --}}
            <h1 class="font-bold text-3xl mb-2">Temario</h1>
            @foreach ($course->sections as $section)
                <article class="mb-4 shadow" 
                    @if($loop->first)
                        x-data="{ open: true }"
                        @else 
                        x-data="{ open: false }"
                    @endif>
                    <header class="border border-gray-200 px-4 py-2 cursor-pointer bg-gray-200" x-on:click="open = !open">
                        <h1 class="font-bold text-lg text-gray-600">{{$section->name}}</h1>
                    </header>
                    <div class="bg-white py-2 px-4" x-show="open">
                        <ul class="grid grid-cols-1 gap-2">
                            @foreach ($section->lessons as $lesson)
                                <li class="text-gray-700 text-base mb-2"> <i class="fas fa-play-circle mr-2 text-gray-600"></i>{{$lesson->name}}</li>
                            @endforeach
                        </ul>
                    </div>
                </article>
            @endforeach

        </section>

        <section class="mb-8 container mt-12">

            <h1 class="font-bold text-3xl">Requisitos</h1>
            <ul class="list-disc list-inside">
                @foreach ($course->requirements as $requirement)
                    <li class="text-gray-700 text-base">{{$requirement->name}}</li>
                @endforeach
            </ul>

        </section>

        <section class="container mb-12">
            <h1 class="font-bold text-3xl">Descripción</h1>
            <div class="text-gray-700 text-base">
                {{$course->description}}
            </div>
        </section>


        <div class="container grid grid-cols-1 gap-6">

            <div class="order-2">
                <section class="card mb-6">
                    <div class="card-body w-96 m-auto">
                        <div class="flex items-center">
                            <img class="h-12 w-12 object-cover rounded-full shadow-lg" src="{{$course->teacher->profile_photo_url}}" alt="{{$course->teacher->name}}">
                            <div class="ml-4">
                                <h1 class="font-bold text-gray-500 text-lg">Prof. {{$course->teacher->name}}</h1>
                                <a class="text-blue-400 text-sm font-bold" href="">{{'@'.Str::slug($course->teacher->name, '')}}</a>
                            </div>
                        </div>

                        @can('enrolled', $course)
                            <a class="w-96 transition-colors duration-200 block px-4 py-2 text-normal text-center mt-3 text-white rounded bg-red-500 hover:bg-red-700 hover:text-white cursor-pointer" href="{{route('courses.status', $course)}}">Continuar con el curso</a>
                        @else
                            <form action="{{route('courses.enrolled', $course)}}" method="post">
                                @csrf
                                <button class="w-96 transition-colors duration-200 block px-4 py-2 text-normal text-center mt-3 text-white rounded bg-red-500 hover:bg-red-700 hover:text-white cursor-pointer" type="submit">Comprar curso</button>
                            </form>
                        @endcan
                        
                       
                    </div>
                </section>

                <aside class="container mb-0 grid grid-cols-1 gap-6 md:grid-cols-5 w-screen">
                    @foreach ($similares as $similar)
                        <article class="flex mb-6">
                            <img class="h-31 w-40 object-cover" src="{{Storage::url($similar->image->url)}}" alt="">
                            <div class="ml-3">
                                <h1>
                                    <a class="font-bold text-gray-500 md:mb-36 " href="{{route('courses.show', $similar)}}">{{Str::limit($similar->title, 40)}}</a>
                                </h1>

                                <div class="flex items-baseline">
                                    <img class="h-8 w-8 object-cover rounded-full shadow-lg" src="{{$similar->teacher->profile_photo_url}}" alt="">
                                    <p class="text-gray-700 text-sm ml-2 mb-10">{{$similar->teacher->name}}</p>
                                </div>

                                <p class="text-sm">
                                    <i class="fas fa-star mr-2 text-yellow-400"></i>
                                    {{$similar->rating}}
                                </p>
                            </div>
                        </article>
                    @endforeach
                </aside>
            </div>

            
            <div class="order-1">

                <section class="card mb-12 py-4 px-6">
                    <div class="card-body">
                        <h1 class="font-bold text-2xl mb-2">Lo que aprenderás</h1>
                    </div>

                    <ul class="grid grid-cols-2 gap-x-6 gap-y-2">
                        @foreach ($course->goals as $goal)
                            <li class="text-gray-700 text-base"> <i class="fas fa-check text-gray-600 mr-2"></i> {{$goal->name}}</li>
                        @endforeach
                    </ul>
                </section>

            </div>

        </div>

</x-app-layout>