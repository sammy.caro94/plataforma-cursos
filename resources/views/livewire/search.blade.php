<form class="pt-2 relative mx-auto text-gray-600" x-data="{ search: '' }" autocomplete="off">
    <div class="w-full flex">
        <input wire:model="search" type="search" class="w-full px-4 py-1 text-gray-900 rounded focus:outline-none"
                placeholder="Buscar" x-model="search">

                
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded relative">
            Buscar
        </button>
    </div>

    @if ($search)
        
        <ul class="absolute z-50 left-0 w-full bg-white mt-1 rounded-lg overflow-hidden">
            @forelse ($this->results as $result)
                <li class="leading-10 text-sm cursor-pointer hover:bg-gray-300 px-5">
                    <a href="{{route('courses.show', $result)}}">{{$result->title}}</a>
                </li>
                @empty
                <li class="leading-10 text-sm cursor-pointer hover:bg-gray-300 px-5">
                    No hay coincidencias :(
                </li>
            @endforelse
        </ul>

    @endif
</form>
