<x-app-layout>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

   {{-- PORTADA --}}
    <section class="bg-cover" style="background-image: url({{asset('img/home/students.jpg')}})">
        
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-36">

            <div class="w-full md:w-3/4 lg:w-1/2">
                <h1 class="text-white font-bold text-4xl">TITULO</h1>
                <p class="text-white text-lg mt-4 mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, similique deleniti a non, distinctio fugit ipsum quas quod corrupti modi, doloribus suscipit ex autem illum facilis iste hic amet atque.</p>

               @livewire('search');
           </div>
       </div>
    </section>

    <section class="mt-24">
        <h1 class="text-gray-600 text-center text-3xl mb-6">CONTENIDO</h1>

        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-x-6 gap-y-8">
            <article>
                <figure>
                    <img class="rounded-xl h-36 w-full object-cover" src="{{asset('img/home/earth.jpg')}}" alt="">
                </figure>

                <header class="mt-2">
                    <h1 class="text-center text-xl text-gray-700">TITULO</h1>
                </header>

                <p class="text-sm text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a laborum tptio delenim vel!</p>
            </article>

            <article>
                <figure>
                    <img class="rounded-xl h-36 w-full object-cover" src="{{asset('img/home/desktop.jpg')}}" alt="">
                </figure>

                <header class="mt-2">
                    <h1 class="text-center text-xl text-gray-700">TITULO</h1>
                </header>

                <p class="text-sm text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a laborum tptio delenim vel!</p>
            </article>

            <article>
                <figure>
                    <img class="rounded-xl h-36 w-full object-cover" src="{{asset('img/home/people.jpg')}}" alt="">
                </figure>

                <header class="mt-2">
                    <h1 class="text-center text-xl text-gray-700">TITULO</h1>
                </header>

                <p class="text-sm text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a laborum tptio delenim vel!</p>
            </article>

            <article>
                <figure>
                    <img class="rounded-xl h-36 w-full object-cover" src="{{asset('img/home/mind.jpg')}}" alt="">
                </figure>

                <header class="mt-2">
                    <h1 class="text-center text-xl text-gray-700">TITULO</h1>
                </header>

                <p class="text-sm text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a laborum tptio delenim vel!</p>
            </article>
        </div>
    </section>

    <section class="mt-24 bg-gray-700 py-12">

        <h1 class="text-center text-white text-3xl">¿No sabes qué curso llevar?</h1>
        <p class="text-center text-white">Dirígete al catálogo de cursos y filtralos por cagegoría o nivel</p>

        <div class="flex justify-center mt-5">
            <a href="{{route('courses.index')}}" type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
                Catálogo de cursos
            </a>
        </div>
    </section>

    <section class="my-24">

        <h1 class="text-center text-3xl text-gray-600">Últimos cursos</h1>
        <p class="text-center text-gray-500 text-sm mb-6">El duro trabajo de nuestros colaboradores para subir cursos</p>

        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-x-6 gap-y-8">

            @foreach ($courses as $course)
                <x-course-card :course='$course'/>
            @endforeach

        </div>

    </section>

</x-app-layout>
