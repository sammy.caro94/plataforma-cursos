@extends('adminlte::page')

@section('title', 'Plataforma Cursos')

@section('content_header')
    <h1>Crear nuevo rol</h1>
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'admin.roles.store']) !!}

                @include('admin.roles.partials.form')

            {!! Form::submit('Crear Rol', ['class'=> 'btn btn-primary mt-2']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('content')
    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop